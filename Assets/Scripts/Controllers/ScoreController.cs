﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ScoreController
{
    private static int score;
    
    public static int Score
    {
        get => score;

        set
        { 
            score = value;
            
            OnScoreChanged?.Invoke(score);
        }
    }


    public static event Action<int> OnScoreChanged;
}
