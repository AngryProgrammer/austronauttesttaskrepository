﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{ 
    public static GameplayManager Instance; 
    
    private void Awake()
    {
        if (Instance == null) { 
            Instance = this; 
        } else if(Instance == this){
            Destroy(gameObject); 
        }

        DontDestroyOnLoad(gameObject);
    }
    
    private void Start()
    {
        MainScreen.OnCountdownOver += StartGame;
        GameTrackableEventHandler.OnTrackingLostEvent += ExitGame;
    }

    private void ExitGame()
    {
        OnGameExit?.Invoke();
    }

    private void StartGame()
    {
        OnGameStart?.Invoke();
    }

    #region Events
    
    public  event Action OnGameStart; 
    public  event Action OnGameExit; 
    
    #endregion

}
