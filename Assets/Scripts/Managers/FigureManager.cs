﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;

/// <summary>
///  Manages all figures work 
/// </summary>
public class FigureManager : MonoBehaviour
{
    public Figure[] figures;

    [SerializeField]
    private GameObject _spawnPoint;
    
    private float _figuresSpeed = 0.2f;
   
    private int _figureSpawnInterval = 2;

    public static FigureManager Instance; 
    
    private void Awake()
    {
        if (Instance == null) { 
            Instance = this; 
        } else if(Instance == this){
            Destroy(gameObject); 
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        GameplayManager.Instance.OnGameStart += SpawnFigures;
        GameplayManager.Instance.OnGameExit += StopSpawnFigures;
    }


    private void SpawnFigures()
    {
        InvokeRepeating(nameof(SpawnObject),0,_figureSpawnInterval);    
        InvokeRepeating(nameof(IncreaseFigureSpeed),Configuration.IncreaseInterval,Configuration.IncreaseInterval); 
    }
    
    private void StopSpawnFigures()
    {
        CancelInvoke();
        
        ScoreController.Score = 0;
        _figuresSpeed = Configuration.FigureSpeed;
    }


    private void SpawnObject()
    {
        var randomFigure = Random.Range(0, figures.Length);
        var randomRotation = Random.rotation;
             
        var randomColor = Random.ColorHSV();

        var figure = Instantiate(figures[randomFigure],_spawnPoint.transform.position,Quaternion.identity);

        figure.Speed = _figuresSpeed;
        figure.Color = randomColor;
        figure.Rotation = randomRotation;
        
        figure.OnFigureDestroyEvent += OnFigureDestruction;
    }

    private void OnFigureDestruction(object obj,EventArgs _args)
    {
        var figure = obj as Figure;

        if (figure == null)
        {
            return;
        }

        var figureCollider =   figure.GetComponent<Collider>(); 
        var figureParticleSystem = figure.GetComponent<ParticleSystem>();
        var figureParticleSystemRenderer = figure.GetComponent<ParticleSystemRenderer>();
        var figureMeshFilter = figure.GetComponent<MeshFilter>();
        var figureRenderer = figure.GetComponent<Renderer>();
        
        figureParticleSystemRenderer.material.color = figure.Color;
        figureParticleSystemRenderer.mesh = figureMeshFilter.mesh;
        figureRenderer.enabled = false; 
        figureCollider.enabled = false;
        
        Destroy(figure.gameObject,figureParticleSystem.main.duration);      
        figureParticleSystem.Play();
        
        ScoreController.Score++;
    }

    private void IncreaseFigureSpeed()
    {
        _figuresSpeed = _figuresSpeed +(_figuresSpeed / 100 * Configuration.FigurePercentIncrease);
    }
}
