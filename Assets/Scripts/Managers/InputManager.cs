﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance; 
    
    private void Awake()
    {
        if (Instance == null) { 
            Instance = this; 
        } else if(Instance == this){
            Destroy(gameObject); 
        }

        DontDestroyOnLoad(gameObject);
    }
    
    void Update()
    {
#if UNITY_EDITOR              
        if (Input.GetMouseButtonUp(0)) 
        {
            RaycastHit hit;
            var sceneCamera = Camera.allCameras[0];

            if (sceneCamera == null) return;
            
            var ray = sceneCamera .ScreenPointToRay(Input.mousePosition);
                    
            if(Physics.Raycast(ray, out hit))
            {
                var figure = hit.transform.gameObject.GetComponent<Figure>();
                
                if(figure == null)
                    return;

                figure.Destruction();
            }
        }
#endif

#if  UNITY_ANDROID
        if (Input.touchCount == 1) 
        {
            RaycastHit hit;

            var sceneCamera = Camera.allCameras[0];
            
            if(sceneCamera != null)
            {           
               var ray = sceneCamera .ScreenPointToRay(Input.GetTouch(0).position);
                        
                if(Physics.Raycast(ray, out hit))
                {
                    var figure = hit.transform.gameObject.GetComponent<Figure>();

                    if (figure == null)
                    {
                        return;
                    }
                    
                
                    Destroy(figure.gameObject);
                    
                    figure.Destruction();
                }
            }
        }
 #endif
    }
}
