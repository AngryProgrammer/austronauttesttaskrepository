﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configuration 
{
   public const float FigureSpeed = 0.2f;

   public const int FigurePercentIncrease = 20;

   public const int IncreaseInterval = 10;
}
