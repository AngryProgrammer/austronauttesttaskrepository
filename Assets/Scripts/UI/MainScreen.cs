﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainScreen : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _scoreText;
    
    [SerializeField]
    private TextMeshProUGUI _countdownText;

    private void Start()
    {
         ScoreController.OnScoreChanged += ChangeScore;
         
         GameTrackableEventHandler.OnTrackingFoundEvent += StartCountdown;
         GameplayManager.Instance.OnGameExit += DisableChildes;
    }

    private void DisableChildes()
    {
       _scoreText.gameObject.SetActive(false);
    }

    private void ChangeScore(int score)
    {
        _scoreText.text = $"{score}";
    }

    private void StartCountdown()
    {
        StartCoroutine(Countdown());
    }

   private IEnumerator Countdown()
    {
        _countdownText.gameObject.SetActive(true);
        
        for (var i = 3; i > 0; i--)
        {
            _countdownText.text = $"{i}";
            
            yield return new WaitForSecondsRealtime(1);
        }

        _countdownText.text = "Start";
        
        yield return new WaitForSecondsRealtime(1);
        
        _countdownText.gameObject.SetActive(false);
        _scoreText.gameObject.SetActive(true);
        
        OnCountdownOver?.Invoke();
    }

   public static event Action OnCountdownOver;
}
