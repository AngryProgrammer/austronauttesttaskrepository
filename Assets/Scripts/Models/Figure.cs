﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour
{
    public float Speed { get; set; }

    public Color Color { get; set; }

    public Quaternion Rotation { get; set; }

    private void Start()
    {
        var figureRenderer = GetComponent<Renderer>();
        
        figureRenderer.material.color = Color;
    }

    // Update is called once per frame
    private void Update()
    {
        transform.Translate(0,0,Speed * Time.deltaTime,Space.World) ;    
        
        transform.rotation =  Quaternion.Slerp(transform.rotation, Rotation, .5f * Time.time);  
    }

    public void Destruction()
    {
        OnFigureDestroyEvent?.Invoke(this,null);
    }

    public event EventHandler OnFigureDestroyEvent;
}
